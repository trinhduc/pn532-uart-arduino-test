# pn532-uart-arduino-test

- Follow the instructions:  https://esp32.com/viewtopic.php?t=4738.
- Make this example on Mac.

## My step

1. Clone this arduino library for the [PN532](https://github.com/elechouse/PN532) to `<install directory>/Arduino/libraries`

2. Edit two line in the file `HardwareSerial.cpp` in `<install directory>\Arduino\hardware\espressif\esp32\cores\esp32\HardwareSerial.cpp`. Can setup esp32 on Arduino IDE as per instruction at [esp32.vn](https://esp32.vn/arduino/install.html#chuan-bi).

```
     if(_uart_nr == 1 && rxPin < 0 && txPin < 0) {
            rxPin = 13;   <---------------------your pin number
            txPin = 15;    <--------------------- your pin number
        }
```

![edit-two-line](assets/edit-two-line-in-HardwareSerialcpp.png)

3. Open Arduino IDE and open example `iso14443a_uid`:

![open-example](assets/open-example-iso14443a.png)

Edit some line or take file main in this project.

4. Config Arduino IDE for upload progam as below:

![config-arduino](assets/config-arduino.png)



